# Extracting useful information from treaty texts

This project develops tools for reproducible, automated, verifiable and interpretable information extraction (IE) from treaty texts for data analysis, ontology population, semantic search, and other uses.

Unlike the related [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) project which only uses [GATE](https://gate.ac.uk), for this project there is both a (Java) GATE and a Python pipeline, and they are not equivalent (yet). Based on my experience developing and using these pipelines, I believe they are complementary -- at least for someone with more familiarity in Python than Java like myself, Python is better for rapid prototyping and exploratory data analysis, while GATE has the advantage of an intuitive graphical user interface for visualizing text annotations, verifying their accuracy in the Lucene search plugin, and displaying the results of automated ontology population. Given that most users and developers of treaty analytics tools are likely to be new to programming, GATE is the more accessible choice because it allows for collaboration at (nearly) all skill levels (from manual annotations to simple code edits and software testing etc.). This means that the GATE app should be the canonical/production version, and any information extraction and transformation that is currently only implemented in Python should be ported to the GATE pipeline. This is work in progress and help is welcome.

The main pattern matching tool in both pipelines is regular expressions (regex) pattern matching, and there are only minor differences between the Java and Python regex modules, depending on the version, most significantly with regard to variable-length [lookbehinds](https://www.regular-expressions.info/lookaround.html#limitbehind)). Regular expressions can thus be tried out and refined in a Python interpreter session (which is almost always faster than running a GATE pipeline), before settling on a version to port to the GATE app for more rigorous testing (in particular checking false positives and false negatives).

Ontology development, logical inference and visualization is best done in [Protégé](https://protege.stanford.edu/), another excellent open source Java-based academic software application. GATE ontology support is limited (see [Chapter 14](https://gate.ac.uk/sale/tao/splitch14.html#x19-35500014) of the User Guide). 

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/

The contents of the repository are:
- `img`: screenshots of the applications
- `parsers`:
  - `textmetadata.jape`: A regex pattern file to be used with the Java Regexp Annotator plugin as a processing resource in a corpus pipeline, capturing treaty text metadata
  - `treatymentions.jape`: A regex pattern file for annotating treaty mentions/topics
  - `treatyprovisions_arts.jape`: A regex pattern file for certain treaty provisions 
  - `treatyIE.jape`: A grammar file to be used with an ontology-aware JAPE Plus Transducer as a processing resource in the same corpus pipeline, populating a PILO database with treaty provisions data
- `examples`:
  - `cleaned-texts`: a set of sample treaty texts, fetched and cleaned (pre-processed) by the [AustLII scraper](https://gitlab.com/legalinformatics/austlii-scraper)
  - `GATE-treatytextsDB`: An Apache Lucene-based searchable datastore (GATE's ANNIC) with the annotated sample treaty texts
  - `GATE-treatytextsDB-index`: Datastore index
  - `urlMap.tsv`: A tab-separated file with URLs in the first column and their corresponding treaty identifier in the second column, for linking information from different sources to the same treaty in the database
  - `pilo_sampletreaties.ttl`: A TURTLE file containing the PILO ontology populated with sample treaty data by GATE
- `GATE-treatytextsIE.xgapp`: A GATE application file with all settings to reproduce the application, but not including external resources (texts, ontology, etc.)
- `GATE-treatytextsIE.zip`: A GATE application bundle including external resources
- `treatytextsIE.org`: Python code and results for sample treaties (best viewed in [Emacs Orgmode](https://orgmode.org/)) [to be added]


## Related projects
- [PILO](https://gitlab.com/legalinformatics/pilo) : Public International Law Ontology in OWL2 format developed for the purpose of data integration
- [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) : GATE application for annotating UNTS treaty records harvested with [UNTS crawler](https://gitlab.com/legalinformatics/unts-crawler) and populating PILO with relevant data (not including texts from UNTS volumes in PDF)
- [AustLII-scraper](https://gitlab.com/legalinformatics/austlii-scraper) : Takes a set of AustLII treaty text URLs and saves the main text content of the HTML pages as TXT files, adding source and retrieval date information, for processing by GATE
- [IEADB-scraper](https://gitlab.com/legalinformatics/ieadb-scraper) : Takes a set of IEADB treaty text URLs and saves the main text content of the HTML pages as TXT files, adding source and retrieval date information, for processing by GATE
- [Treaty-identifiers](https://gitlab.com/legalinformatics/treaty-identifiers) : Automated matching and merging of UNTS and IEADB records by using a combination of treaty title and adoption date ('titlenad') also used in `urlMap.tsv` for treaty lookup during ontology population


## Installation instructions

### GATE app

Prerequisites:
1. Check whether you already have a suitable Java version installed (ideally the 64bit version of Java 11 or later)[^1]
   -> enter "java -version" on a Linux, Mac, or Windows terminal/command prompt 
2. Download and install GATE Developer 9.0.1 from https://gate.ac.uk/download/ (choose installer based on your operating system)
   -> (on Linux) run installer with $java -jar gate-developer-9.0.1-installer.jar
3. Start GATE Developer

Open the application using the provided ZIP archive:
1. Download and extract `treatytextsIE.zip` to a suitable location
2. In GATE Developer, click on "File" -> "Restore Application from File...", navigate to the extracted folder and select `application.xgapp`
3. Run application and check results

Running the application is very simple, double-click on it to open it, make sure all the components are there (and PILO is selected for ontology population), and run it by clicking on 'Run this Application' at the bottom of the window. Here's a screenshot:

![GATE-treatytextsIE-app](img/GATE-treatytextsIE-app.png "GATE treatytextsIE app")

A sample treaty text annotated by the app looks like this (showing annotation list at the bottom and annotation set selection on the right):

![GATE-treatytextsIE-POPconv](img/GATE-treatytextsIE-POPconv.png "GATE treatytextsIE for POP Convention")

A view of the Lucene Datastore Searcher with 'TechTransfer' chosen as the annotation type to search for:

![GATE-treatytextsIE-mentions-LuceneSearch](img/GATE-treatytextsIE-mentions-LuceneSearch.png "Screenshot of TechTransfer annotation search")

The size of the context window (number of tokens to show from the left and right side of the match) can be adjusted with the 'Context size' slider at the top right corner, and the matches with contexts and metadata can be exported to an HTML file by clicking 'Export' at the top of the results table.

Provisions can be searched and exported similarly:

![GATE-treatytextsIE-provs-LuceneSearch](img/GATE-treatytextsIE-provs-LuceneSearch.png "Screenshot of withdrawal provision search")

To give an example of how the GATE and Python pipelines differ, in the Python pipeline temporal variables (like the two years minimum waiting period before denunciation in the preceding screenshot) are converted to the same time unit (years) for comparison. Also, number words in the specification of entry into force conditions (e.g. entry into force after the 'twentieth' ratification) are converted to numbers and added as a numeric variable, whereas in the GATE pipeline they are merely extracted for now. Enhancing the GATE app to reach the level of information extraction as the Python pipeline is high on my to-do list, but will have to wait completion of other pressing tasks.

### Python pipeline
Python code and results are embedded in the [Emacs Orgmode](https://orgmode.org/) file `treatytextsIE.org`. In principle the code blocks could be extracted into a separate script (or a Jupyter Notebook) for those who would rather not use Emacs, but that would require additional effort and time I cannot spare at the moment. Using Emacs orgmode has many advantages over other options and would highly recommend checking it out. Data/variables can be passed from an org table to a code block, and from one programming language to another within the same org file, and the number supported languages is very [impressive](https://orgmode.org/worg/org-contrib/babel/languages/index.html).

Prerequisites:
1. Install or update [GNU Emacs](https://www.gnu.org/software/emacs/download.html)) -- I have Emacs 27.1 with Orgmode 9.5, but probably anything from Emacs 24 with Orgmode 8 would work
2. Install or update [Python](https://www.python.org/downloads/) -- I have version 3.9 but any version >= 3.5 would probably work
3. Enable Orgmode support for Python in your Emacs configuration file, adding it to the list of languages to load (see [instructions](https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html))
4. Install Python packages for code blocks you want to run (can also be installed as you go, whenever the code throws an error) -- ideally with the system-wide package manager (like apt in Debian/Ubuntu) because they're more secure, tailored and integrated with the rest of the OS than add-on package managers like Anaconda or PyPi

GNU Emacs and Python are installed as standard on many GNU Linux and Mac OS systems, but it may be worth double-checking whether there is an update available for installation. On Debian or Debian-derived systems like Ubuntu attempting an update would be as easy as:

```
sudo apt update && sudo apt install emacs python3
```


[^1]: Technically GATE only requires Java 8 or later, but there may have been changes in Java's regex engine between versions 8 and 11 (which was used for developing this application).
